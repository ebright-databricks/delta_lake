# Databricks notebook source
# MAGIC %md ##### Import Libraries

# COMMAND ----------

dbutils.library.installPyPI('numpy', '1.20.0')
dbutils.library.installPyPI('pandas', '1.2.4')
dbutils.library.restartPython()

# COMMAND ----------

# MAGIC %md ##### Custom Libraries

# COMMAND ----------

MAGIC %run "./delta_lake_utils"

# COMMAND ----------

# MAGIC %md #### Custom Functions

# COMMAND ----------

random_udf = udf(lambda: str(uuid.uuid4()), StringType()).asNondeterministic()

# COMMAND ----------


# MAGIC %md ##### Unit Test Classes

# COMMAND ----------

# MAGIC %md ###### Overwrite Parquet File

# COMMAND ----------

from datetime import datetime, timedelta
from pyspark.sql.functions import udf, from_json, col, explode, concat_ws, max, lit,current_timestamp, expr
from pyspark.sql.types import TimestampType, IntegerType, StringType
from pyspark.sql.dataframe import DataFrame
from delta.tables import DeltaTable
import pandas as pd
import numpy
import uuid
import unittest
class TestParquetOverwrite(unittest.TestCase):
  
  def setUp(self):
    self.container = "lakehouse"
    self.data_phase = "unit_test"
    self.database = "unit_test"
    self.source = "gwog"
    self.object = "person"
    self.table = f"src__{self.source}__{self.object}"
    self.today = datetime.today()
    self.dates = {}
    self.dates["today_date_iso"] = self.today.strftime("%Y-%m-%d")
    self.dates["year"] = self.today.year
    self.dates["month"] = f"{self.today.month:02}"
    self.dates["day"] = f"{self.today.day:02}"
    self.year = self.dates["year"]
    self.month = self.dates["month"]
    self.day = self.dates["day"]
    self.primary_key = "id"
    self.surrogate_key_list = ["id", "name"]
    self.today_datetime_iso = self.today.strftime("%Y-%m-%d")
    self.parquet_location = f"/mnt/{self.container}/{self.data_phase}/{self.source}/raw/{self.object}"
    self.parquet_location_sk_list = f"/mnt/{self.container}/{self.data_phase}/{self.source}/raw_sk_list/{self.object}"
    self.delta_location = f"/mnt/{self.container}/{self.data_phase}/{self.source}/{self.object}"
    self.test_name = "unit_test_ParquetOverwrite"
    self.src_filepath = f"{self.parquet_location}/{self.object}_{self.test_name}.snappy.parquet"
    self.src_sk_list_filepath = f"{self.parquet_location_sk_list}/{self.object}_{self.test_name}.snappy.parquet"
    # DDL - Create table if not exists
    spark.sql(f"CREATE DATABASE IF NOT EXISTS {self.data_phase}")
    
  def test_parquet_overwrite(self):
    persons = [
      (1, "bob", 1),
      (2, "bill", 1),
      (3, "mary", 2)
    ]
    headers = ["id", "name", "type"]
    df_src = spark.createDataFrame(data=persons, schema=headers)
    df_src.write.format("parquet").save(self.src_filepath)
      
    persons_sk_list = [
      (2, "bill"),
      (3, "mary")
    ]
    headers_sk_list = ["id", "name"]
    df_src_sk_list = spark.createDataFrame(data=persons_sk_list, schema=headers_sk_list)
    df_src_sk_list.write.format("parquet").save(self.src_sk_list_filepath)
    overwrite_parquet_file(df_src_sk_list, self.src_filepath)
    df_src = spark.read.parquet(self.src_filepath)
    df_target_as_pd = df_src.select(["id", "name"]).sort("id").toPandas()
    expected_output_df = pd.DataFrame([
      {"id": 2, "name": "bill"},
      {"id": 3, "name": "mary"}
    ])
    self.assertTrue(numpy.array_equal(expected_output_df.to_numpy(na_value='null'), df_target_as_pd.to_numpy(na_value='null')))
  def tearDown(self):
    # DDL - Remove resources
    spark.sql(f"DROP TABLE IF EXISTS {self.data_phase}.{self.table}")
    dbutils.fs.rm(self.parquet_location, True)
    dbutils.fs.rm(self.parquet_location_sk_list, True)

# COMMAND ----------

suite = unittest.TestLoader().loadTestsFromTestCase(TestParquetOverwrite)
runner = unittest.TextTestRunner(verbosity=2)
runner.run(suite)

# COMMAND ----------

# MAGIC %md ###### Add slcd2 columns

# COMMAND ----------

from datetime import datetime, timedelta
import pyspark.sql.functions as F
from pyspark.sql.types import *
from pyspark.sql.dataframe import DataFrame
from delta.tables import DeltaTable
import pandas as pd
import numpy
import uuid
import unittest
class TestDeltaLoadAddSCD2Columns(unittest.TestCase):
  
  def setUp(self):
    self.container = "lakehouse"
    self.data_phase = "unit_test"
    self.database = "unit_test"
    self.source = "gwog"
    self.object = "person"
    self.table = f"src__{self.source}__{self.object}"
    self.today = datetime.today()
    self.dates = {}
    self.dates["today_date_iso"] = self.today.strftime("%Y-%m-%d")
    self.dates["year"] = self.today.year
    self.dates["month"] = f"{self.today.month:02}"
    self.dates["day"] = f"{self.today.day:02}"
    self.year = self.dates["year"]
    self.month = self.dates["month"]
    self.day = self.dates["day"]
    self.primary_key = "id"
    self.today_datetime_iso = self.today.strftime("%Y-%m-%d")
    self.test_name = "unit_test_insert"
    self.parquet_location = f"/mnt/{self.container}/{self.data_phase}/{self.source}/raw/{self.object}"
    self.delta_location = f"/mnt/{self.container}/{self.data_phase}/{self.source}/{self.object}"
    self.src_filepath = f"{self.parquet_location}/{self.object}_{self.test_name}.snappy.parquet"
    
    # DDL - Create table if not exists
    spark.sql(f"CREATE DATABASE IF NOT EXISTS {self.data_phase}")
    
  def test_add_scd2_columns(self):
    persons = [
      (1, "bob", 1),
      (2, "bill", 1),
      (3, "mary", None)
    ]
    headers = ["id", "name", "type"]
    df_src = spark.createDataFrame(data=persons, schema=headers)
    df_target = add_scd2_columns(df_src, headers)
    expected_output = ['id', 'name', 'type', 'timestamp_start', 'timestamp_load', 'is_current', 'timestamp_end','primary_key', 'surrogate_key']
    self.assertTrue(df_target.columns, expected_output)
    
  def tearDown(self):
    # DDL - Remove resources
    spark.sql(f"DROP TABLE IF EXISTS {self.data_phase}.{self.table}")

# COMMAND ----------

suite = unittest.TestLoader().loadTestsFromTestCase(TestDeltaLoadAddSCD2Columns)
runner = unittest.TextTestRunner(verbosity=2)
runner.run(suite)

# COMMAND ----------

# MAGIC %md ###### Add Surrogate Key

# COMMAND ----------

from datetime import datetime, timedelta
import pyspark.sql.functions as F
from pyspark.sql.types import *
from pyspark.sql.dataframe import DataFrame
from delta.tables import DeltaTable
import pandas as pd
import numpy
import uuid
import unittest
class TestDeltaLoadAddSurrogateKey(unittest.TestCase):
  
  def setUp(self):
    self.container = "lakehouse"
    self.data_phase = "unit_test"
    self.database = "unit_test"
    self.source = "gwog"
    self.object = "person"
    self.table = f"src__{self.source}__{self.object}"
    self.today = datetime.today()
    self.dates = {}
    self.dates["today_date_iso"] = self.today.strftime("%Y-%m-%d")
    self.dates["year"] = self.today.year
    self.dates["month"] = f"{self.today.month:02}"
    self.dates["day"] = f"{self.today.day:02}"
    self.year = self.dates["year"]
    self.month = self.dates["month"]
    self.day = self.dates["day"]
    self.primary_key = "id"
    self.today_datetime_iso = self.today.strftime("%Y-%m-%d")
    self.test_name = "unit_test_insert"
    self.parquet_location = f"/mnt/{self.container}/{self.data_phase}/{self.source}/raw/{self.object}"
    self.delta_location = f"/mnt/{self.container}/{self.data_phase}/{self.source}/{self.object}"
    self.src_filepath = f"{self.parquet_location}/{self.object}_{self.test_name}.snappy.parquet"
    
    # DDL - Create table if not exists
    spark.sql(f"CREATE DATABASE IF NOT EXISTS {self.data_phase}")
    
  def test_add_surrogate_key(self):
    persons = [
      (1, "bob", 1),
      (2, "bill", 1),
      (3, "mary", None)
    ]
    headers = ["id", "name", "type"]
    surrogate_key_list = ["id"]
    df_src = spark.createDataFrame(data=persons, schema=headers)
    df_target = add_scd2_columns(df_src, surrogate_key_list)
    df_target_as_pd = df_target.select(["id", "surrogate_key"]).sort("id").toPandas()
    expected_output_df = pd.DataFrame([
      {"id": 1, "surrogate_key": "356a192b7913b04c54574d18c28d46e6395428ab"},
      {"id": 2, "surrogate_key": "da4b9237bacccdf19c0760cab7aec4a8359010b0"},
      {"id": 3, "surrogate_key": "77de68daecd823babbb58edb1c8e14d7106e83bb"}
    ])
    self.assertTrue(numpy.array_equal(expected_output_df.to_numpy(na_value='null'), df_target_as_pd.to_numpy(na_value='null')))
    
  def tearDown(self):
    # DDL - Remove resources
    spark.sql(f"DROP TABLE IF EXISTS {self.data_phase}.{self.table}")

# COMMAND ----------

suite = unittest.TestLoader().loadTestsFromTestCase(TestDeltaLoadAddSurrogateKey)
runner = unittest.TextTestRunner(verbosity=2)
runner.run(suite)

# COMMAND ----------

# MAGIC %md ###### Add Surrogate Key - Multiple Fields

# COMMAND ----------

from datetime import datetime, timedelta
import pyspark.sql.functions as F
from pyspark.sql.types import *
from pyspark.sql.dataframe import DataFrame
from delta.tables import DeltaTable
import pandas as pd
import numpy
import uuid
import unittest
class TestDeltaLoadAddSurrogateKeyMultipleFields(unittest.TestCase):
  
  def setUp(self):
    self.container = "lakehouse"
    self.data_phase = "unit_test"
    self.database = "unit_test"
    self.source = "gwog"
    self.object = "person"
    self.table = f"src__{self.source}__{self.object}"
    self.today = datetime.today()
    self.dates = {}
    self.dates["today_date_iso"] = self.today.strftime("%Y-%m-%d")
    self.dates["year"] = self.today.year
    self.dates["month"] = f"{self.today.month:02}"
    self.dates["day"] = f"{self.today.day:02}"
    self.year = self.dates["year"]
    self.month = self.dates["month"]
    self.day = self.dates["day"]
    self.primary_key = "id"
    self.today_datetime_iso = self.today.strftime("%Y-%m-%d")
    self.test_name = "unit_test_insert"
    self.parquet_location = f"/mnt/{self.container}/{self.data_phase}/{self.source}/raw/{self.object}"
    self.delta_location = f"/mnt/{self.container}/{self.data_phase}/{self.source}/{self.object}"
    self.src_filepath = f"{self.parquet_location}/{self.object}_{self.test_name}.snappy.parquet"
    
    # DDL - Create table if not exists
    spark.sql(f"CREATE DATABASE IF NOT EXISTS {self.data_phase}")
    
  def test_add_surrogate_key(self):
    persons = [
      (1, "bob", 1),
      (2, "bill", 1),
      (3, "mary", None)
    ]
    headers = ["id", "name", "type"]
    surrogate_key_list = ["id", "name"]
    df_src = spark.createDataFrame(data=persons, schema=headers)
    df_target = add_scd2_columns(df_src, surrogate_key_list)
    df_target_as_pd = df_target.select(["id", "surrogate_key"]).sort("id").toPandas()
    expected_output_df = pd.DataFrame([
      {"id": 1, "surrogate_key": "940597a250571f6bcd14b19f0bbbc38711a49f8e"},
      {"id": 2, "surrogate_key": "53a9fc159b9933a26357e67f9a12387b7109551d"},
      {"id": 3, "surrogate_key": "d4583e440c87238218de3e7a86ad318c315a14f6"}
    ])
    self.assertTrue(numpy.array_equal(expected_output_df.to_numpy(na_value='null'), df_target_as_pd.to_numpy(na_value='null')))
    
  def tearDown(self):
    # DDL - Remove resources
    spark.sql(f"DROP TABLE IF EXISTS {self.data_phase}.{self.table}")

# COMMAND ----------

suite = unittest.TestLoader().loadTestsFromTestCase(TestDeltaLoadAddSurrogateKeyMultipleFields)
runner = unittest.TextTestRunner(verbosity=2)
runner.run(suite)

# COMMAND ----------

# MAGIC %md ###### Process Schema Changes

# COMMAND ----------

from datetime import datetime, timedelta
import pyspark.sql.functions as F
from pyspark.sql.types import *
from pyspark.sql.dataframe import DataFrame
from delta.tables import DeltaTable
import pandas as pd
import numpy
import uuid
import unittest
class TestDeltaLoadProcessSchemaChanges(unittest.TestCase):
  
  def setUp(self):
    self.container = "lakehouse"
    self.data_phase = "unit_test"
    self.database = "unit_test"
    self.source = "gwog"
    self.object = "person"
    self.table = f"src__{self.source}__{self.object}"
    self.today = datetime.today()
    self.dates = {}
    self.dates["today_date_iso"] = self.today.strftime("%Y-%m-%d")
    self.dates["year"] = self.today.year
    self.dates["month"] = f"{self.today.month:02}"
    self.dates["day"] = f"{self.today.day:02}"
    self.year = self.dates["year"]
    self.month = self.dates["month"]
    self.day = self.dates["day"]
    self.primary_key = "id"
    self.today_datetime_iso = self.today.strftime("%Y-%m-%d")
    self.test_name = "unit_test_insert"
    self.parquet_location = f"/mnt/{self.container}/{self.data_phase}/{self.source}/raw/{self.object}"
    self.delta_location = f"/mnt/{self.container}/{self.data_phase}/{self.source}/{self.object}"
    self.src_filepath = f"{self.parquet_location}/{self.object}_{self.test_name}.snappy.parquet"
    
    # DDL - Create table if not exists
    spark.sql(f"CREATE DATABASE IF NOT EXISTS {self.data_phase}")
    
  def test_process_schema_changes(self):
    persons = [
      (1, "bob", 1),
      (2, "bill", 1),
      (3, "mary", 2)
    ]
    headers = ["id", "name", "type"]
    df_src = spark.createDataFrame(data=persons, schema=headers)
    df_src.write.format("parquet").mode("overwrite").save(self.src_filepath)
    
    df_src = spark.read.parquet(src_filepath)

    df_src = add_scd2_columns(df_src, surrogate_key_list)

    df_dst = create_delta_table(spark, df_src, data_phase, database, table, delta_location)

    df_dst = process_schema_changes(df_src, df_dst, data_phase, database, table, spark, delta_location)
    
    persons_upsert = [
      (1, "bob", 1),
      (2, "bill", None),
      (3, "mary", 2),
      (4, "bro", 1)
    ]
    headers_upsert = ["id", "name", "type"]
    df_src_upsert = spark.createDataFrame(data=persons_upsert, schema=headers_upsert)
    df_src_upsert.write.format("parquet").mode("overwrite").save(self.src_filepath_upsert)
    merge_upsert(self.container, self.data_phase, self.source, self.database, self.object, self.year, self.month, self.day, self.today_datetime_iso, self.surrogate_key_list, self.src_filepath_upsert)
  
    df_target = DeltaTable.forPath(spark, self.delta_location)
    df_target_as_pd = df_target.toDF().select(["id", "name", "type", "is_current"]).where("is_current = 1").sort("id").toPandas()
    expected_output_df = pd.DataFrame([
      {"id": 1, "name": "bob", "type": 1, "is_current": 1},
      {"id": 2, "name": "bill", "type": "null", "is_current": 1},
      {"id": 3, "name": "mary", "type": 2, "is_current": 1},
      {"id": 4, "name": "bro", "type": 1, "is_current": 1}
    ])
    self.assertTrue(numpy.array_equal(expected_output_df.to_numpy(na_value='null'), df_target_as_pd.to_numpy(na_value='null')))
    
  def tearDown(self):
    # DDL - Remove resources
    spark.sql(f"DROP TABLE IF EXISTS {self.data_phase}.{self.table}")

# COMMAND ----------

# MAGIC %md ###### Merge - Insert

# COMMAND ----------

from datetime import datetime, timedelta
from pyspark.sql.functions import udf, from_json, col, explode, concat_ws, max, lit,current_timestamp, expr
from pyspark.sql.types import TimestampType, IntegerType, StringType
from pyspark.sql.dataframe import DataFrame
from delta.tables import DeltaTable
import pandas as pd
import numpy
import uuid
import unittest
class TestDeltaLoadMergeInsert(unittest.TestCase):
  
  def setUp(self):
    self.container = "lakehouse"
    self.data_phase = "unit_test"
    self.source = "gwog"
    self.object = "person"
    self.database = "unit_test"
    self.table = f"src__{self.source}__{self.object}"
    self.today = datetime.today()
    self.dates = {}
    self.dates["today_date_iso"] = self.today.strftime("%Y-%m-%d")
    self.dates["year"] = self.today.year
    self.dates["month"] = f"{self.today.month:02}"
    self.dates["day"] = f"{self.today.day:02}"
    self.year = self.dates["year"]
    self.month = self.dates["month"]
    self.day = self.dates["day"]
    self.surrogate_key_list = ["id"]
    self.today_datetime_iso = self.today.strftime("%Y-%m-%d")
    self.test_name = "unit_test_insert"
    self.parquet_location = f"/mnt/{self.container}/{self.data_phase}/{self.source}/raw/{self.object}"
    self.delta_location = f"/mnt/{self.container}/{self.data_phase}/{self.source}/{self.object}"
    self.src_filepath = f"{self.parquet_location}/{self.object}_{self.test_name}.snappy.parquet"
    
    # DDL - Create table if not exists
    spark.sql(f"CREATE DATABASE IF NOT EXISTS {self.data_phase}")
    
  def test_merge_insert(self):
    persons = [
      (1, "bob", 1),
      (2, "bill", 1),
      (3, "mary", 2)
    ]
    headers = ["id", "name", "type"]
    df_src = spark.createDataFrame(data=persons, schema=headers)
   
    df_src.write.format("parquet").mode("overwrite").save(self.src_filepath)
    merge_upsert(container=self.container, data_phase=self.data_phase, source=self.source, database=self.database, object=self.object, year=self.year, month=self.month, day=self.day, today_datetime_iso=self.today_datetime_iso, surrogate_key_list=self.surrogate_key_list, src_filepath=self.src_filepath)
    
    df_target = DeltaTable.forPath(spark, self.delta_location)
    df_target_as_pd = df_target.toDF().select(["id", "name", "type", "is_current"]).sort("id").toPandas()
    expected_output_df = pd.DataFrame([
      {"id": 1, "name": "bob", "type": 1, "is_current": 1},
      {"id": 2, "name": "bill", "type": 1, "is_current": 1},
      {"id": 3, "name": "mary", "type": 2, "is_current": 1}
    ])
    df_target.toDF().show(truncate=False)
    self.assertTrue(numpy.array_equal(expected_output_df.to_numpy(na_value='null'), df_target_as_pd.to_numpy(na_value='null')))
    
  def tearDown(self):
    # DDL - Remove resources
    spark.sql(f"DROP TABLE IF EXISTS {self.data_phase}.{self.table}")
    dbutils.fs.rm(self.parquet_location, True)
    dbutils.fs.rm(self.delta_location, True)

# COMMAND ----------

suite = unittest.TestLoader().loadTestsFromTestCase(TestDeltaLoadMergeInsert)
runner = unittest.TextTestRunner(verbosity=2)
runner.run(suite)

# COMMAND ----------

# MAGIC %md ###### Merge - Insert with SK List

# COMMAND ----------

from datetime import datetime, timedelta
from pyspark.sql.functions import udf, from_json, col, explode, concat_ws, max, lit,current_timestamp, expr
from pyspark.sql.types import TimestampType, IntegerType, StringType
from pyspark.sql.dataframe import DataFrame
from delta.tables import DeltaTable
import pandas as pd
import numpy
import uuid
import unittest
class TestDeltaLoadMergeInsertSKList(unittest.TestCase):
  
  def setUp(self):
    self.container = "lakehouse"
    self.data_phase = "unit_test"
    self.source = "gwog"
    self.object = "person"
    self.database = "unit_test"
    self.table = f"src__{self.source}__{self.object}"
    self.today = datetime.today()
    self.dates = {}
    self.dates["today_date_iso"] = self.today.strftime("%Y-%m-%d")
    self.dates["year"] = self.today.year
    self.dates["month"] = f"{self.today.month:02}"
    self.dates["day"] = f"{self.today.day:02}"
    self.year = self.dates["year"]
    self.month = self.dates["month"]
    self.day = self.dates["day"]
    self.surrogate_key_list = ["id", "name"]
    self.today_datetime_iso = self.today.strftime("%Y-%m-%d")
    self.test_name = "unit_test_insert"
    self.parquet_location = f"/mnt/{self.container}/{self.data_phase}/{self.source}/raw/{self.object}"
    self.delta_location = f"/mnt/{self.container}/{self.data_phase}/{self.source}/{self.object}"
    self.src_filepath = f"{self.parquet_location}/{self.object}_{self.test_name}.snappy.parquet"
    
    # DDL - Create table if not exists
    spark.sql(f"CREATE DATABASE IF NOT EXISTS {self.data_phase}")
    
  def test_merge_insert(self):
    persons = [
      (1, "bob", 1),
      (2, "bill", 1),
      (3, "mary", 2)
    ]
    headers = ["id", "name", "type"]
    df_src = spark.createDataFrame(data=persons, schema=headers)
   
    df_src.write.format("parquet").mode("overwrite").save(self.src_filepath)
    merge_upsert(container=self.container, data_phase=self.data_phase, source=self.source, database=self.database, object=self.object, year=self.year, month=self.month, day=self.day, today_datetime_iso=self.today_datetime_iso, surrogate_key_list=self.surrogate_key_list, src_filepath=self.src_filepath)
    
    df_target = DeltaTable.forPath(spark, self.delta_location)
    df_target_as_pd = df_target.toDF().select(["id", "name", "type", "is_current"]).sort("id").toPandas()
    expected_output_df = pd.DataFrame([
      {"id": 1, "name": "bob", "type": 1, "is_current": 1},
      {"id": 2, "name": "bill", "type": 1, "is_current": 1},
      {"id": 3, "name": "mary", "type": 2, "is_current": 1}
    ])
    df_target.toDF().show(truncate=False)
    self.assertTrue(numpy.array_equal(expected_output_df.to_numpy(na_value='null'), df_target_as_pd.to_numpy(na_value='null')))
    
  def tearDown(self):
    # DDL - Remove resources
    spark.sql(f"DROP TABLE IF EXISTS {self.data_phase}.{self.table}")
    dbutils.fs.rm(self.parquet_location, True)
    dbutils.fs.rm(self.delta_location, True)

# COMMAND ----------

suite = unittest.TestLoader().loadTestsFromTestCase(TestDeltaLoadMergeInsertSKList)
runner = unittest.TextTestRunner(verbosity=2)
runner.run(suite)

# COMMAND ----------

# MAGIC %md ###### Merge - Upsert

# COMMAND ----------

from datetime import datetime, timedelta
from pyspark.sql.functions import udf, from_json, col, explode, concat_ws, max, lit,current_timestamp, expr
from pyspark.sql.types import TimestampType, IntegerType, StringType
from pyspark.sql.dataframe import DataFrame
from delta.tables import DeltaTable
import pandas as pd
import numpy
import uuid
import unittest
class TestDeltaLoadMergeUpsert(unittest.TestCase):
  
  def setUp(self):
    self.container = "lakehouse"
    self.data_phase = "unit_test"
    self.database = "unit_test"
    self.source = "gwog"
    self.object = "person"
    self.table = f"src__{self.source}__{self.object}"
    self.today = datetime.today()
    self.dates = {}
    self.dates["today_date_iso"] = self.today.strftime("%Y-%m-%d")
    self.dates["year"] = self.today.year
    self.dates["month"] = f"{self.today.month:02}"
    self.dates["day"] = f"{self.today.day:02}"
    self.year = self.dates["year"]
    self.month = self.dates["month"]
    self.day = self.dates["day"]
    self.surrogate_key_list = ["id"]
    self.today_datetime_iso = self.today.strftime("%Y-%m-%d")
    self.test_name = "unit_test_upsert"
    self.parquet_location = f"/mnt/{self.container}/{self.data_phase}/{self.source}/raw/{self.object}"
    self.delta_location = f"/mnt/{self.container}/{self.data_phase}/{self.source}/{self.object}"
    self.src_filepath = f"{self.parquet_location}/{self.object}_{self.test_name}.snappy.parquet"
    self.src_filepath_upsert = f"{self.parquet_location}/{self.object}_{self.test_name}_upsert.snappy.parquet"
    
    # DDL - Create table if not exists
    spark.sql(f"CREATE DATABASE IF NOT EXISTS {self.data_phase}")
    
  def test_merge_upsert(self):
    persons = [
      (1, None, 1),
      (2, "bill", 1),
      (3, "mary", 2)
    ]
    headers = ["id", "name", "type"]
    df_src = spark.createDataFrame(data=persons, schema=headers)
    df_src.write.format("parquet").mode("overwrite").save(self.src_filepath)
    merge_upsert(self.container, self.data_phase, self.source, self.database, self.object, self.year, self.month, self.day, self.today_datetime_iso, self.surrogate_key_list, self.src_filepath)
    
    persons_upsert = [
      (1, "bob", 1),
      (2, "bill", None),
      (3, "mary", 2),
      (4, "bro", 1)
    ]
    headers_upsert = ["id", "name", "type"]
    df_src_upsert = spark.createDataFrame(data=persons_upsert, schema=headers_upsert)
    df_src_upsert.write.format("parquet").mode("overwrite").save(self.src_filepath_upsert)
    merge_upsert(self.container, self.data_phase, self.source, self.database, self.object, self.year, self.month, self.day, self.today_datetime_iso, self.surrogate_key_list, self.src_filepath_upsert)
  
    df_target = DeltaTable.forPath(spark, self.delta_location)
    df_target_as_pd = df_target.toDF().select(["id", "name", "type", "is_current"]).where("is_current = 1").sort("id").toPandas()
#     df_target_as_pd = df_target.toDF().select(["id", "name", "type", "is_current"]).sort("id").toPandas()
    expected_output_df = pd.DataFrame([
      {"id": 1, "name": "bob", "type": 1, "is_current": 1},
      {"id": 2, "name": "bill", "type": "null", "is_current": 1},
      {"id": 3, "name": "mary", "type": 2, "is_current": 1},
      {"id": 4, "name": "bro", "type": 1, "is_current": 1}
    ])
    self.assertTrue(numpy.array_equal(expected_output_df.to_numpy(na_value='null'), df_target_as_pd.to_numpy(na_value='null')))
    
  def tearDown(self):
    # DDL - Remove resources
    spark.sql(f"DROP TABLE IF EXISTS {self.data_phase}.{self.table}")
    dbutils.fs.rm(self.parquet_location, True)
    dbutils.fs.rm(self.delta_location, True)

# COMMAND ----------

suite = unittest.TestLoader().loadTestsFromTestCase(TestDeltaLoadMergeUpsert)
runner = unittest.TextTestRunner(verbosity=2)
runner.run(suite)

# COMMAND ----------

# MAGIC %md ###### Merge - Add Column

# COMMAND ----------

from datetime import datetime, timedelta
from pyspark.sql.functions import udf, from_json, col, explode, concat_ws, max, lit,current_timestamp, expr
from pyspark.sql.types import TimestampType, IntegerType, StringType
from pyspark.sql.dataframe import DataFrame
from delta.tables import DeltaTable
import pandas as pd
import numpy
import uuid
import unittest
class TestDeltaLoadMergeAddColumn(unittest.TestCase):
  
  def setUp(self):
    self.container = "lakehouse"
    self.data_phase = "unit_test"
    self.database = "unit_test"
    self.source = "gwog"
    self.object = "person"
    self.table = f"src__{self.source}__{self.object}"
    self.today = datetime.today()
    self.dates = {}
    self.dates["today_date_iso"] = self.today.strftime("%Y-%m-%d")
    self.dates["year"] = self.today.year
    self.dates["month"] = f"{self.today.month:02}"
    self.dates["day"] = f"{self.today.day:02}"
    self.year = self.dates["year"]
    self.month = self.dates["month"]
    self.day = self.dates["day"]
    self.primary_key = "id"
    self.surrogate_key_list = ["id"]
    self.today_datetime_iso = self.today.strftime("%Y-%m-%d")
    self.test_name = "unit_test_addcolumn"
    self.parquet_location = f"/mnt/{self.container}/{self.data_phase}/{self.source}/raw/{self.object}"
    self.delta_location = f"/mnt/{self.container}/{self.data_phase}/{self.source}/{self.object}"
    self.src_filepath = f"{self.parquet_location}/{self.object}_{self.test_name}.snappy.parquet"
    self.src_filepath_add_column = f"{self.parquet_location}/{self.object}_{self.test_name}_upsert.snappy.parquet"
    
    # DDL - Create table if not exists
    spark.sql(f"CREATE DATABASE IF NOT EXISTS {self.data_phase}")
    
  def test_merge_add_column(self):
    persons = [
      (1, "bob", 1),
      (2, "bill", 1),
      (3, "mary", 2)
    ]
    headers = ["id", "name", "type"]
    df_src = spark.createDataFrame(data=persons, schema=headers)
    df_src.write.format("parquet").mode("overwrite").save(self.src_filepath)
    
    merge_upsert(self.container, self.data_phase, self.source, self.database, self.object, self.year, self.month, self.day, self.today_datetime_iso, self.surrogate_key_list, self.src_filepath)
      
    persons_add_column = [
      (1, "bob", 1, "alive"),
      (2, "bill", 1, "dead"),
      (3, "mary", 2, "alive")
    ]
    headers_add_column = ["id", "name", "type", "status"]
    df_src_add_column = spark.createDataFrame(data=persons_add_column, schema=headers_add_column)
    df_src_add_column.write.format("parquet").mode("overwrite").save(self.src_filepath_add_column)
    
    merge_upsert(self.container, self.data_phase, self.source, self.database, self.object, self.year, self.month, self.day, self.today_datetime_iso, self.surrogate_key_list, self.src_filepath_add_column)
      
    df_target = DeltaTable.forPath(spark, self.delta_location)
    df_target_as_pd = df_target.toDF().select(["id", "name", "type", "status"]).where("is_current = 1").sort("id").toPandas()
    expected_output_df = pd.DataFrame([
      {"id": 1, "name": "bob", "type": 1, "status": "alive"},
      {"id": 2, "name": "bill", "type": 1,  "status": "dead"},
      {"id": 3, "name": "mary", "type": 2,  "status": "alive"}
    ])
    self.assertTrue(numpy.array_equal(expected_output_df.to_numpy(na_value='null'), df_target_as_pd.to_numpy(na_value='null')))
    
  def tearDown(self):
    # DDL - Remove resources
    spark.sql(f"DROP TABLE IF EXISTS {self.data_phase}.{self.table}")
    dbutils.fs.rm(self.parquet_location, True)
    dbutils.fs.rm(self.delta_location, True)

# COMMAND ----------

suite = unittest.TestLoader().loadTestsFromTestCase(TestDeltaLoadMergeAddColumn)
runner = unittest.TextTestRunner(verbosity=2)
runner.run(suite)

# COMMAND ----------

# MAGIC %md ###### Merge - Insert with Add Column

# COMMAND ----------

from datetime import datetime, timedelta
from pyspark.sql.functions import udf, from_json, col, explode, concat_ws, max, lit,current_timestamp, expr
from pyspark.sql.types import TimestampType, IntegerType, StringType
from pyspark.sql.dataframe import DataFrame
from delta.tables import DeltaTable
import pandas as pd
import numpy
import uuid
import unittest
class TestDeltaLoadMergeInsertAddColumn(unittest.TestCase):
  
  def setUp(self):
    self.container = "lakehouse"
    self.data_phase = "unit_test"
    self.database = "unit_test"
    self.source = "gwog"
    self.object = "person"
    self.table = f"src__{self.source}__{self.object}"
    self.today = datetime.today()
    self.dates = {}
    self.dates["today_date_iso"] = self.today.strftime("%Y-%m-%d")
    self.dates["year"] = self.today.year
    self.dates["month"] = f"{self.today.month:02}"
    self.dates["day"] = f"{self.today.day:02}"
    self.year = self.dates["year"]
    self.month = self.dates["month"]
    self.day = self.dates["day"]
    self.primary_key = "id"
    self.surrogate_key_list = ["id"]
    self.today_datetime_iso = self.today.strftime("%Y-%m-%d")
    self.test_name = "unit_test_InsertAddColumn"
    self.parquet_location = f"/mnt/{self.container}/{self.data_phase}/{self.source}/raw/{self.object}"
    self.delta_location = f"/mnt/{self.container}/{self.data_phase}/{self.source}/{self.object}"
    self.src_filepath = f"{self.parquet_location}/{self.object}_{self.test_name}.snappy.parquet"
    self.src_filepath_insert_add_column = f"{self.parquet_location}/{self.object}_{self.test_name}_upsert.snappy.parquet"
    
    # DDL - Create table if not exists
    spark.sql(f"CREATE DATABASE IF NOT EXISTS {self.data_phase}")
    
  def test_merge_insert_add_column(self):
    persons = [
      (1, "bob", 1),
      (2, "bill", 1),
      (3, "mary", 2)
    ]
    headers = ["id", "name", "type"]
    df_src = spark.createDataFrame(data=persons, schema=headers)
    df_src.write.format("parquet").mode("overwrite").save(self.src_filepath)
    
    merge_upsert(self.container, self.data_phase, self.source, self.database, self.object, self.year, self.month, self.day, self.today_datetime_iso, self.surrogate_key_list, self.src_filepath)
    
    persons_insert_add_column = [
      (1, "bob", 1, "alive"),
      (2, "bill", 1, "dead"),
      (3, "mary", 2, "alive"),
      (4, "bro", 1, "alive")
    ]
    headers_insert_add_column = ["id", "name", "type", "status"]
    df_src_insert_add_column = spark.createDataFrame(data=persons_insert_add_column, schema=headers_insert_add_column)
    df_src_insert_add_column.write.format("parquet").mode("overwrite").save(self.src_filepath_insert_add_column)
      
    merge_upsert(self.container, self.data_phase, self.source, self.database, self.object, self.year, self.month, self.day, self.today_datetime_iso, self.surrogate_key_list, self.src_filepath_insert_add_column)
      
    df_target = DeltaTable.forPath(spark, self.delta_location)
    df_target_as_pd = df_target.toDF().select(["id", "name", "type", "status"]).where("is_current = 1").sort("id").toPandas()
    expected_output_df = pd.DataFrame([
      {"id": 1, "name": "bob", "type": 1, "status": "alive"},
      {"id": 2, "name": "bill", "type": 1,  "status": "dead"},
      {"id": 3, "name": "mary", "type": 2,  "status": "alive"},
      {"id": 4, "name": "bro", "type": 1, "status": "alive"}
    ])
    self.assertTrue(numpy.array_equal(expected_output_df.to_numpy(na_value='null'), df_target_as_pd.to_numpy(na_value='null')))
    
  def tearDown(self):
    # DDL - Remove resources
    spark.sql(f"DROP TABLE IF EXISTS {self.data_phase}.{self.table}")
    dbutils.fs.rm(self.parquet_location, True)
    dbutils.fs.rm(self.delta_location, True)

# COMMAND ----------

suite = unittest.TestLoader().loadTestsFromTestCase(TestDeltaLoadMergeInsertAddColumn)
runner = unittest.TextTestRunner(verbosity=2)
runner.run(suite)

# COMMAND ----------

# MAGIC %md ###### Merge - Update with Add Column

# COMMAND ----------

from datetime import datetime, timedelta
from pyspark.sql.functions import udf, from_json, col, explode, concat_ws, max, lit,current_timestamp, expr
from pyspark.sql.types import TimestampType, IntegerType, StringType
from pyspark.sql.dataframe import DataFrame
from delta.tables import DeltaTable
import pandas as pd
import numpy
import uuid
import unittest
class TestDeltaLoadMergeUpdateAddColumn(unittest.TestCase):
  
  def setUp(self):
    self.container = "lakehouse"
    self.data_phase = "unit_test"
    self.database = "unit_test"
    self.source = "gwog"
    self.object = "person"
    self.table = f"src__{self.source}__{self.object}"
    self.today = datetime.today()
    self.dates = {}
    self.dates["today_date_iso"] = self.today.strftime("%Y-%m-%d")
    self.dates["year"] = self.today.year
    self.dates["month"] = f"{self.today.month:02}"
    self.dates["day"] = f"{self.today.day:02}"
    self.year = self.dates["year"]
    self.month = self.dates["month"]
    self.day = self.dates["day"]
    self.primary_key = "id"
    self.surrogate_key_list = ["id"]
    self.today_datetime_iso = self.today.strftime("%Y-%m-%d")
    self.test_name = "unit_test_UpdateAddColumn"
    self.parquet_location = f"/mnt/{self.container}/{self.data_phase}/{self.source}/raw/{self.object}"
    self.delta_location = f"/mnt/{self.container}/{self.data_phase}/{self.source}/{self.object}"
    self.src_filepath = f"{self.parquet_location}/{self.object}_{self.test_name}.snappy.parquet"
    self.src_filepath_update_add_column = f"{self.parquet_location}/{self.object}_{self.test_name}_upsert.snappy.parquet"
    
    # DDL - Create table if not exists
    spark.sql(f"CREATE DATABASE IF NOT EXISTS {self.data_phase}")
    
  def test_merge_update_add_column(self):
    persons = [
      (1, "bob", 1),
      (2, "bill", 1),
      (3, "mary", 2)
    ]
    headers = ["id", "name", "type"]
    df_src = spark.createDataFrame(data=persons, schema=headers)
    df_src.write.format("parquet").mode("overwrite").save(self.src_filepath)
    
    merge_upsert(self.container, self.data_phase, self.source, self.database, self.object, self.year, self.month, self.day, self.today_datetime_iso, self.surrogate_key_list, self.src_filepath)
    
    persons_update_add_column = [
      (1, "bob", 3, "alive"),
      (2, "bill", 1, "dead"),
      (3, "mary", 2, "alive")
    ]
    headers_update_add_column = ["id", "name", "type", "status"]
    df_src_update_add_column = spark.createDataFrame(data=persons_update_add_column, schema=headers_update_add_column)
    df_src_update_add_column.write.format("parquet").mode("overwrite").save(self.src_filepath_update_add_column)
    
    merge_upsert(self.container, self.data_phase, self.source, self.database, self.object, self.year, self.month, self.day, self.today_datetime_iso, self.surrogate_key_list, self.src_filepath_update_add_column)
     
    df_target = DeltaTable.forPath(spark, self.delta_location)
    df_target_as_pd = df_target.toDF().select(["id", "name", "type", "status"]).where("is_current = 1").sort("id").toPandas()
    expected_output_df = pd.DataFrame([
      {"id": 1, "name": "bob", "type": 3, "status": "alive"},
      {"id": 2, "name": "bill", "type": 1,  "status": "dead"},
      {"id": 3, "name": "mary", "type": 2,  "status": "alive"}
    ])
    self.assertTrue(numpy.array_equal(expected_output_df.to_numpy(na_value='null'), df_target_as_pd.to_numpy(na_value='null')))
    
  def tearDown(self):
    # DDL - Remove resources
    spark.sql(f"DROP TABLE IF EXISTS {self.data_phase}.{self.table}")
    dbutils.fs.rm(self.parquet_location, True)
    dbutils.fs.rm(self.delta_location, True)

# COMMAND ----------

suite = unittest.TestLoader().loadTestsFromTestCase(TestDeltaLoadMergeUpdateAddColumn)
runner = unittest.TextTestRunner(verbosity=2)
runner.run(suite)

# COMMAND ----------

# MAGIC %md ###### Merge - Soft Delete

# COMMAND ----------

from datetime import datetime, timedelta
from pyspark.sql.functions import udf, from_json, col, explode, concat_ws, max, lit,current_timestamp, expr
from pyspark.sql.types import TimestampType, IntegerType, StringType
from pyspark.sql.dataframe import DataFrame
from delta.tables import DeltaTable
import pandas as pd
import numpy
import uuid
import unittest
class TestDeltaLoadMergeSoftDelete(unittest.TestCase):
  
  def setUp(self):
    self.container = "lakehouse"
    self.data_phase = "unit_test"
    self.database = "unit_test"
    self.source = "gwog"
    self.object = "person"
    self.table = f"src__{self.source}__{self.object}"
    self.today = datetime.today()
    self.dates = {}
    self.dates["today_date_iso"] = self.today.strftime("%Y-%m-%d")
    self.dates["year"] = self.today.year
    self.dates["month"] = f"{self.today.month:02}"
    self.dates["day"] = f"{self.today.day:02}"
    self.year = self.dates["year"]
    self.month = self.dates["month"]
    self.day = self.dates["day"]
    self.primary_key = "id"
    self.surrogate_key_list = ["id", "name"]
    self.today_datetime_iso = self.today.strftime("%Y-%m-%d")
    self.parquet_location = f"/mnt/{self.container}/{self.data_phase}/{self.source}/raw/{self.object}"
    self.parquet_location_sk_list = f"/mnt/{self.container}/{self.data_phase}/{self.source}/raw_sk_list/{self.object}"
    self.delta_location = f"/mnt/{self.container}/{self.data_phase}/{self.source}/{self.object}"
    self.test_name = "unit_test_SoftDelete"
    self.src_filepath = f"{self.parquet_location}/{self.object}_{self.test_name}.snappy.parquet"
    self.src_sk_list_filepath = f"{self.parquet_location_sk_list}/{self.object}_{self.test_name}.snappy.parquet"
    # DDL - Create table if not exists
    spark.sql(f"CREATE DATABASE IF NOT EXISTS {self.data_phase}")
    
  def test_merge_soft_delete(self):
    persons = [
      (1, "bob", 1),
      (2, "bill", 1),
      (3, "mary", 2)
    ]
    headers = ["id", "name", "type"]
    df_src = spark.createDataFrame(data=persons, schema=headers)
    df_src.write.format("parquet").mode("overwrite").save(self.src_filepath)
    merge_upsert(self.container, self.data_phase, self.source, self.database, self.object, self.year, self.month, self.day, self.today_datetime_iso, self.surrogate_key_list, self.src_filepath)
      
    persons_sk_list = [
      (2, "bill"),
      (3, "mary")
    ]
    headers_sk_list = ["id", "name"]
    df_src_sk_list = spark.createDataFrame(data=persons_sk_list, schema=headers_sk_list)
    df_src_sk_list.write.format("parquet").mode("overwrite").save(self.src_sk_list_filepath)
    merge_soft_delete(self.delta_location, self.surrogate_key_list, self.src_sk_list_filepath)
    df_target = DeltaTable.forPath(spark, self.delta_location)
    df_target_as_pd = df_target.toDF().select(["id", "name", "type", "is_current"]).sort("id").toPandas()
    expected_output_df = pd.DataFrame([
      {"id": 1, "name": "bob", "type": 1, "is_current": 0},
      {"id": 2, "name": "bill", "type": 1, "is_current": 1},
      {"id": 3, "name": "mary", "type": 2, "is_current": 1}
    ])
    self.assertTrue(numpy.array_equal(expected_output_df.to_numpy(na_value='null'), df_target_as_pd.to_numpy(na_value='null')))
  def tearDown(self):
    # DDL - Remove resources
    spark.sql(f"DROP TABLE IF EXISTS {self.data_phase}.{self.table}")
    dbutils.fs.rm(self.parquet_location, True)
    dbutils.fs.rm(self.delta_location, True)

# COMMAND ----------

suite = unittest.TestLoader().loadTestsFromTestCase(TestDeltaLoadMergeSoftDelete)
runner = unittest.TextTestRunner(verbosity=2)
runner.run(suite)

# COMMAND ----------

# MAGIC %md ##### Run Tests

# COMMAND ----------

import unittest
loader = unittest.TestLoader()
suite = unittest.TestSuite()
suite.addTests(loader.loadTestsFromTestCase(TestDeltaLoadAddSCD2Columns))
suite.addTests(loader.loadTestsFromTestCase(TestDeltaLoadAddSurrogateKey))
suite.addTests(loader.loadTestsFromTestCase(TestDeltaLoadAddSurrogateKeyMultipleFields))
suite.addTests(loader.loadTestsFromTestCase(TestDeltaLoadMergeInsert))
suite.addTests(loader.loadTestsFromTestCase(TestDeltaLoadMergeUpsert))
suite.addTests(loader.loadTestsFromTestCase(TestDeltaLoadMergeAddColumn))
suite.addTests(loader.loadTestsFromTestCase(TestDeltaLoadMergeInsertAddColumn))
suite.addTests(loader.loadTestsFromTestCase(TestDeltaLoadMergeUpdateAddColumn))
suite.addTests(loader.loadTestsFromTestCase(TestDeltaLoadMergeSoftDelete))


runner = unittest.TextTestRunner(verbosity=2)
runner.run(suite)