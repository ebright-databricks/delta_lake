# Databricks notebook source
# MAGIC %md Spark Configuration

# COMMAND ----------

spark.conf.set('spark.sql.session.timeZone', 'MST')

# COMMAND ----------

# MAGIC %md  import libraries

# COMMAND ----------

from datetime import datetime, timedelta
from pyspark.sql.functions import udf, from_json, col, explode, concat_ws, max, lit,current_timestamp, expr
from pyspark.sql.types import TimestampType, IntegerType, StringType
from delta.tables import *
import uuid

# COMMAND ----------

# MAGIC %md Custom Functions

# COMMAND ----------

random_udf = udf(lambda: str(uuid.uuid4()), StringType()).asNondeterministic()

# COMMAND ----------

MAGIC %run "./delta_lake_utils"

# COMMAND ----------

dbutils.widgets.text("source", "")
dbutils.widgets.text("object", "")
dbutils.widgets.text("year", "")
dbutils.widgets.text("month", "")
dbutils.widgets.text("day", "")
dbutils.widgets.text("surrogate_key_list", "")
dbutils.widgets.text("today_datetime_iso", "")

container = "lakehouse"
environment = "dev"
data_phase = "source"
source = dbutils.widgets.get("source")
object = dbutils.widgets.get("object")
database = f"src_{source}"
table = f"{object}"
year = dbutils.widgets.get("year")
month = dbutils.widgets.get("month")
day = dbutils.widgets.get("day")
surrogate_key_list = dbutils.widgets.get("surrogate_key_list").split(',')
today_datetime_iso = dbutils.widgets.get("today_datetime_iso")
delta_location = f"/mnt/{container}/{data_phase}/{source}/{object}"
src_filepath = f"/mnt/{container}/raw/{source}/{object}/{year}/{month}/{day}/{object}_{today_datetime_iso}.snappy.parquet"

# COMMAND ----------

# COMMAND ----------

# MAGIC %md ##### Read Parquet file

# COMMAND ----------

merge_upsert(container=container, data_phase=data_phase, source=source, database=database, object=object, year=year, month=month, day=day, today_datetime_iso=today_datetime_iso, surrogate_key_list=surrogate_key_list, src_filepath=src_filepath)
