# Databricks notebook source
from datetime import datetime, timedelta
import pyspark.sql.functions as F
from pyspark.sql.types import *
from delta.tables import *
import uuid
import os

# COMMAND ----------

random_udf = F.udf(lambda: str(uuid.uuid4()), StringType()).asNondeterministic()

# COMMAND ----------

# MAGIC %md ###### add_surrogate_key

# COMMAND ----------

def add_surrogate_key(df, surrogate_key_list):
  surrogate_key_list = [key.strip() for key in surrogate_key_list]
  df_src = df.withColumn("surrogate_key", F.sha1(F.concat_ws(';', *[F.coalesce(F.regexp_replace(c, pattern="\s+", replacement=""), F.lit('')) for c in surrogate_key_list])))
  return df_src

# COMMAND ----------

# MAGIC %md ###### add_scd2_columns

# COMMAND ----------

def add_scd2_columns(df, surrogate_key_list):
  surrogate_key_list = [key.strip() for key in surrogate_key_list]
  df_src = df.withColumn("timestamp_start", F.current_timestamp().cast("timestamp"))
  df_src = df_src.withColumn("timestamp_load", F.current_timestamp().cast("timestamp"))
  df_src = df_src.withColumn("is_current", F.lit(1).cast("integer"))
  df_src = df_src.withColumn("timestamp_end", F.lit(None).cast("timestamp"))
  df_src = df_src.withColumn("primary_key", F.expr("uuid()"))
  df_src = df_src.withColumn("surrogate_key", F.sha1(F.concat_ws(';', *[F.coalesce(F.regexp_replace(c, pattern="\s+", replacement=""), F.lit('')) for c in surrogate_key_list])))
  return df_src

# COMMAND ----------

# MAGIC %md ###### create_delta_table

# COMMAND ----------

def create_delta_table(spark, df_src, data_phase, database, table, delta_location):
  # DDL - Create database if not exists
  spark.sql(f"CREATE DATABASE IF NOT EXISTS {database}")

  # Check if delta table exists, if not create
  if(DeltaTable.isDeltaTable(spark, delta_location) == False):
    df_src.write.format("delta").save(delta_location)

  # DDL - Create table if not exists
  spark.sql(f"CREATE TABLE IF NOT EXISTS {database}.{table} USING DELTA LOCATION '{delta_location}'")

  # Load delta table
  delta_table_dst = DeltaTable.forPath(spark, delta_location)

  return delta_table_dst

# COMMAND ----------

# MAGIC %md ###### process_schema_changes

# COMMAND ----------

def process_schema_changes(dt_src, dt_dst, data_phase, database, table, spark, delta_location):
  # Process schema changes
  added_column_list = [x for x in dt_src.dtypes if all(y not in x[0] for y in dt_dst.toDF().columns)]
  # Add column if new in source
  if len(added_column_list) > 0:
    for column_tuple in added_column_list:
      spark.sql(f"ALTER TABLE {database}.{table} ADD COLUMNS ( {column_tuple[0]} {column_tuple[1]})")
    # Refresh Table
    dt_dst = DeltaTable.forPath(spark, delta_location)
  return dt_dst

# COMMAND ----------

# MAGIC %md ###### inserts_updates

# COMMAND ----------

## Rows to insert records that have updates to existing records in source
def inserts_updates(dt_src, dt_dst):
  ## WHERE clause updates
  column_exclude = ['timestamp_start', 'timestamp_load', 'is_current', 'timestamp_end', 'primary_key', 'surrogate_key', 'merge_key']
  
  ## build list of non SCD2 columns
  update_column_list = [x for x in dt_dst.toDF().columns if all(y not in x for y in column_exclude)]

  # Create list from only columns that are in destination and source
  update_column_list = list(set(update_column_list) & set(dt_src.columns))

  column_update_string = []
  for column in update_column_list:
    # Use ifnull with dummy string in order to properly handle null evaluation (fails on non simple types, ex timestamp)
    column_update_string.append(f" ifnull(existing.{column}, 'null') <> ifnull(inserts_updates.{column}, 'null') OR ")

    # Compare using NULL safe operator but fails on non null equality
#     column_update_string.append(f" existing.{column} <=> inserts_updates.{column} OR ")
  
      # compare on not equal operator
#     column_update_string.append(f" existing.{column} != inserts_updates.{column} OR ")
    where = "".join(column_update_string)
  where_existing = where[:len(where)-3]

  # Inserts Updates Select Expression
  select_inserts_updates = []
  for column in dt_src.columns:
    select_inserts_updates.append(f"inserts_updates.{column}")

  ## should only pull records that were updated in source and matched with destination
  inserts_updates = (
    dt_src
      .alias("inserts_updates")
      .join(dt_dst.toDF().alias("existing")
            , on="surrogate_key", how="inner")
      .where(f"existing.is_current = 1 AND ({where_existing})")
      .selectExpr(select_inserts_updates)
  )
  return inserts_updates

# COMMAND ----------

# MAGIC %md ###### inserts_new

# COMMAND ----------

## Rows to insert net new records
def inserts_new(df_src, df_dst):
  # Inserts New Select Expression
  select_inserts_new = []
  for column in df_src.columns:
    select_inserts_new.append(f"inserts_new.{column}")

  inserts_new = (
    df_src
    .alias("inserts_new")
    .join(df_dst.toDF().alias("existing"), on="surrogate_key", how="left_outer")
    .where(f"existing.surrogate_key IS NULL")
    .selectExpr(select_inserts_new)
  )
  return inserts_new

# COMMAND ----------

# MAGIC %md ###### updates

# COMMAND ----------

## Rows to update the scd2 is_current and timestamp_end columns after inserts
def updates(df_src, df_dst, inserts_updates):
  # Updates Select Expression
  select_updates = []
  for column in df_src.columns:
    select_updates.append(f"updates.{column}")

  updates = (
    df_src
    .alias("updates")
    .join(inserts_updates.alias("existing"), on="surrogate_key", how="inner")
    .where(f"existing.is_current = 1")
    .selectExpr(select_updates)
  )
  return updates

# COMMAND ----------

# MAGIC %md ###### staged_updates

# COMMAND ----------

def staged_updates(inserts_updates, inserts_new, updates):
  ## Final dataframe for merge
  staged_updates = (
    inserts_updates
    .selectExpr(f"NULL as merge_key", "*")
    .union(
      inserts_new.selectExpr(f"surrogate_key as merge_key", "*")
    )
    .union(
      updates.selectExpr(f"surrogate_key as merge_key", "*")
    )
  )
  return staged_updates

# COMMAND ----------

def staged_updates_value_dict(staged_updates):
  column_exclude = ['merge_key']
  ## build list of common columns
  final_column_list = [x for x in staged_updates.columns if all(y not in x for y in column_exclude)]
  value_statement_dict = {}
  for column in final_column_list:
    value_statement_dict[column] = (f"staged_updates.{column}")
  return value_statement_dict

# COMMAND ----------

# MAGIC %md ###### merge_delta

# COMMAND ----------

def merge_delta(df_dst, staged_updates,staged_updates_value_dict):

  (df_dst.alias("existing")
    .merge(
      staged_updates.alias("staged_updates"),
      f"existing.surrogate_key = staged_updates.merge_key")
     .whenMatchedUpdate(
       set = {
         "is_current" : lit(0).cast("integer"),
         "timestamp_end": current_timestamp().cast("timestamp") 
       } 
     )
     .whenNotMatchedInsertAll()
#      .whenNotMatchedInsert(
#        values = staged_updates_value_dict
#      )
     .execute())

# COMMAND ----------

# MAGIC %md ###### delete_delta

# COMMAND ----------

def delete_delta(df_src, df_dst):
  df_delete = df_dst.toDF().join(df_src, [df_dst.toDF()["surrogate_key"] == df_src["surrogate_key"]], how='left_anti').groupby("surrogate_key").count().selectExpr(f"surrogate_key")
  (df_dst.alias("target")
  .merge(
    df_delete.alias("source"),
    f"target.surrogate_key = source.surrogate_key")
   .whenMatchedUpdate(
     condition = "target.is_current = 1",
     set = {
       "is_current" : F.lit(0).cast("integer"),
       "timestamp_end": F.current_timestamp().cast("timestamp") 
     } 
   )
   .execute())
  # Return spark data frame
  return df_delete

# COMMAND ----------

# MAGIC %md ###### Overwrite Parquet File

# COMMAND ----------

def overwrite_parquet_file(df, src_filepath):
  # Generate temp filepath
  src_filepath_split = src_filepath.split(".")
  src_filepath_temp = f"{src_filepath_split[0]}_temp.{src_filepath_split[1]}.{src_filepath_split[2]}"
  
  # Write df to new parquet file
  df.write.format("parquet").save(src_filepath_temp)
  
  # Delete original file
  dbutils.fs.rm(src_filepath, True)
  
  # Rename new file
  dbutils.fs.mv(src_filepath_temp, src_filepath, True)

# COMMAND ----------

# MAGIC %md ###### merge_upsert

# COMMAND ----------

def merge_upsert(container, data_phase, source, database, object, year, month, day, today_datetime_iso, surrogate_key_list, src_filepath):
  table = f"{object}"
  
  delta_location = f"/mnt/{container}/{data_phase}/{source}/{object}"
  
  # Read parquet file from path, return spark data frame
  df_src = spark.read.parquet(src_filepath)

  # add scd2 columns, return spark data frame
  df_src = add_scd2_columns(df_src, surrogate_key_list)
  
  # create delta table, return delta table
  df_dst = create_delta_table(spark, df_src, data_phase, database, table, delta_location)
  
  # Process schema changes, return delta table
  df_dst = process_schema_changes(df_src, df_dst, data_phase, database, table, spark, delta_location)
  
  # Rows to insert records that have updates to existing records in source, returns spark data frame
  ins_upd = inserts_updates(df_src, df_dst)

  # Inserts for net new records, returns spark data frame
  ins_new = inserts_new(df_src, df_dst)

  # updates to scd2 records for updated records, return spark data frame
  upd = updates(df_src, df_dst, ins_upd)

  # combine records from insert updates, inserts new and updates
  stg_updates = staged_updates(ins_upd, ins_new, upd)

  stg_upd_value_dict = staged_updates_value_dict(stg_updates)
  
  merge_delta(df_dst, stg_updates, stg_upd_value_dict)

# COMMAND ----------

# MAGIC %md ###### merge_soft_delete

# COMMAND ----------

## TO DO need to rebuild delete pipeline for primary key list and surrogate key

def merge_soft_delete(delta_location, surrogate_key_list, src_filepath):
  df_src = spark.read.parquet(src_filepath)
  df_src = add_surrogate_key(df_src, surrogate_key_list)
  df_dst = DeltaTable.forPath(spark, delta_location)
  new_df = delete_delta(df_src, df_dst)
  overwrite_parquet_file(new_df, src_filepath)

# COMMAND ----------

# MAGIC %md ###### update_delta_from_date_range

# COMMAND ----------

def update_delta_from_date_range(container, data_phase, source, object, start_date, end_date, surrogate_key):
  start_date = datetime.fromisoformat(start_date)
  end_date = datetime.fromisoformat(end_date)
  delta = timedelta(days=1)
  while start_date <= end_date:

    # Loop through all parquet files in 
    for root, dirs, files in os.walk(f"/dbfs/mnt/{container}/{data_phase}/{source}/{object}/{start_date.year}/{start_date.month:02}/{start_date.day:02}"):
      for name in files:
        filepath = os.path.join(root, name).strip("/dbfs")
        filepath = f"/{filepath}"
#         merge_upsert(container, data_phase, source, object, year, month, day, today_datetime_iso, surrogate_key, filepath)
    start_date += delta

# COMMAND ----------

## TO DO: add function for deletes for date range

# COMMAND ----------

# update_delta_from_date_range("lakehouse", "raw", "wellandexport", "cc", "2021-04-12", "2021-05-21", "id")
# for root, dirs, files in os.walk(f"/dbfs/mnt/lakehouse/raw/wellandexport/cc/2021/05/02"):
#   print(files)
#   for name in files:
#     filepath = os.path.join(root, name).strip("/dbfs")
#     print(f"/{filepath}")
